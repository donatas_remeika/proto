// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "P2GameModeBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class PROTOTYPE2_API AP2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
		AP2GameModeBase();
};
