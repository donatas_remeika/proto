// Fill out your copyright notice in the Description page of Project Settings.

#include "P2GameModeBase.h"
#include "FighterP1.h"
#include "FighterController.h"

AP2GameModeBase::AP2GameModeBase()
{
	PlayerControllerClass = AFighterController::StaticClass();
	DefaultPawnClass = AFighterP1::StaticClass();
}
