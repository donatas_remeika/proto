// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FighterController.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE2_API AFighterController : public APlayerController
{
	GENERATED_BODY()
		AFighterController();
};
