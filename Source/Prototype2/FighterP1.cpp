// Fill out your copyright notice in the Description page of Project Settings.

#include "FighterP1.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerController.h"
#include "FighterController.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"


// Sets default values
AFighterP1::AFighterP1()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFighterP1::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFighterP1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFighterP1::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("Run", this, &AFighterP1::Run);
	InputComponent->BindAxis("Strafe", this, &AFighterP1::Strafe);
	InputComponent->BindAxis("Turn", this, &AFighterP1::Turn);
}

void AFighterP1::Run(float amount) {
	if (Controller && amount) {
		AddMovementInput(GetActorForwardVector(), amount);
	}
}

void AFighterP1::Strafe(float amount) {
	if (Controller && amount) {
		AddMovementInput(GetActorRightVector(), amount);
	}
}

void AFighterP1::Turn(float amount) {
	/*for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{

	}*/
	//FIXME this has to get proper player as in the future there will be more players
	APlayerController* ctrl = GetWorld()->GetFirstPlayerController();

	FVector mouseWorldLocation, mouseDirection;
	ctrl->DeprojectMousePositionToWorld(mouseWorldLocation, mouseDirection);

	FVector MouseLocationEnd;
	FVector actorLoc = GetActorLocation();
	FHitResult HitResult;

	MouseLocationEnd = (mouseDirection * 10000) + mouseWorldLocation;

	//Stores settings for raycast.
	FCollisionQueryParams TraceSettings;

	FCollisionResponseParams TraceResponses;

	//Takes GroundPlane by GameTraceChannel1 accoring to Default.ini 
	ETraceTypeQuery MyTraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);
	ECollisionChannel customCollision = ECollisionChannel::ECC_GameTraceChannel2;
	
	// Do trace.If it succeeds, complete calculations and set new rotation.
	if (GetWorld()->LineTraceSingleByChannel(HitResult, mouseWorldLocation, MouseLocationEnd, customCollision, TraceSettings,
		TraceResponses) == true)
	{
		DrawDebugLine(
			GetWorld(),
			actorLoc,
			HitResult.Location,
			FColor(255, 0, 0),
			false, -1, 0,
			12.333
		);

		//Calculate new rotation with actor's location and hit result.
		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(actorLoc, HitResult.ImpactPoint);
		SetActorRotation(FRotator(GetControlRotation().Pitch, NewRotation.Yaw, GetControlRotation().Roll));
	}
}